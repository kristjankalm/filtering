## Required libraries
* https://github.com/circstat/circstat-matlab
* https://gitlab.com/kristjankalm/libDirectional-kalm
* https://github.com/DrosteEffect/BrewerMap
* https://gitlab.com/kristjankalm/utils
* https://github.com/altmany/export_fig
* https://gitlab.com/kristjankalm/plot_tools

For the recency bias analysis run _rb/_bf.m 