function fit_params
Model = 'cdm';
%Model = 'vmm'; % mix with uniform
Data = 'fw_s1';
ParamFit = ['fa_' Model '_' Data];
ef = bf('Model', Model, 'Data', Data);

% % uniform & natprior
R  = pi/23.6;
m = 12;
std_q = roundn(pi./linspace(12, 30, m),-2);
beta = roundn(linspace(0.3, 0.85, m),-2);

do_filter = 0;
do_param_plot = 1;
do_bias_plot = 0;

%% filtering
if do_filter

    [y, dy, NumBins, dy_edges, fw_curve] = get_vars(ef);
    
    % parfor variables
    Rss = nan(m,m,m);
       
    tic
    for i = 1:m
        
        for q = 1:size(std_q,2)
            
            Q  = std_q(q); % in std
            
            parfor b = 1:size(beta,2)
                
                ff = bf('Model', ef.Model, 'Data', ef.Data, 'R', R, 'Q', Q, 'np', 10^3, 'beta', beta(b));

                r = ff.Filtering;
                Er = circ_dist(r, y); % response minus y
                
                Eb = bf.GetBinData(dy, Er(2:end), NumBins, dy_edges);
                Em = cellfun(@circ_mean, Eb);
                Rss(q,b,i) = sum(circ_dist(fw_curve, Em).^2);
                
                %E{q,b} = Er;
            end
        end

    end    
    save(fullfile(ef.Folder.Mat, ParamFit), 'Rss');
    
    disp('--'); toc   
    
    min(Rss(:))
end

%% parameters plot
if do_param_plot
    
    close all
    % load average
    ParamFit = ['fa_' Model '_' Data];
    load(fullfile(ef.Folder.Mat, ParamFit), 'Rss');
    
    Rss_mean = mean(Rss,3);
    [i,j,k] = ind2sub(size(Rss), find(Rss == min(Rss(:))));
    
    Rss_min = Rss(:,:,k);
    
    h = figure('Position', [100 100 650 350]);
    set(gcf, 'Color', [1 1 1]);
    FontSize = 18;
    
    imagesc(Rss_mean, [0.015 0.1]);
    colorbar;
    
    do_degrees = 1;
    std_q = sqrt(1 ./ std_q); % kappa -> std
    std_q = std_q ./ 2;     % convert to FW space
    kappa_q = 1 ./ (std_q.^2); % std -> kappa
    if do_degrees
        kappa_q = round(circ_rad2ang(kappa_q));
    end
    
    %std_q = std_q ./ 2; % convert to FW space
    %kappa_q = round(1./(std_q.^2),1);
    beta_lab = beta(1:2:end);
    set(gca, ...
        'YTickLabel', kappa_q(2:2:end), ...
        'XTick', 1:2:size(beta,2), ...
        'XTickLabel', beta_lab, ...
        'FontSize', FontSize);

    xlabel('\beta', 'FontSize', FontSize);
    hy = ylabel('\kappa_Q', 'FontSize', FontSize);
    pos = get(hy,'pos'); % Read position [x y z]
    set(hy,'pos',pos+[-0.1 0 0]) % Move label to right
    
    FileName = fullfile(ef.Folder.Fig, ParamFit);
    export_fig(FileName, '-pdf');
end

%% bias plot
if do_bias_plot

    %load(fullfile(ef.Folder.Mat, ParamFit), 'Rss');
    %Rss = Rss(:,:,12);
    %[q,b] = ind2sub(size(Rss), find(Rss == min(Rss(:))));

    q = 3;
    b = 10;

    std_q(q)
    kappa_q = round(1./(std_q(q).^2),1)
    beta(b)
    %Rss(q,b)
    
    
    ff = bf('Model', Model, 'Data', Data, 'R', R, 'Q', std_q(q), 'beta', beta(b));
    disp(ff.Label)
    h = figure('Position', [50 100 600 500]);
    ff.PlotBias;
    FileName = fullfile(ef.Folder.Fig, ['bias_' ef.Model])
    export_fig(FileName, '-pdf');
end

end

function [y, dy, NumBins, dy_edges, fw_curve] = get_vars(ef)
load(fullfile(ef.Folder.Data, ['data_' ef.Data '.mat']), 'y');
n = 1;

NumBins = 13;
binsize = 2*pi/NumBins;
dy = circ_dist(y(1:end-n), y(n+1:end));
dy_edges = linspace(-pi+binsize, pi, NumBins);
increment = diff(dy_edges(1:2)) / 2; % the shift to the mean of the bin on the plot
xe = dy_edges - increment;

% these values are derived from girshick_fit_vm.m
F = ef.GetFunctions;
% F.GaussDiff = @(x, a, w, c) x .* a .* w .* (sqrt(2)./exp(-0.5)) .*exp(-(w.*x).^2) + c;
% first derivative of Gaussian
% a     amplitude
% w     width
% c     intercept

% fisher and whitney average DOG parameters
load(fullfile(ef.Folder.Stats, 'stat_fw_dog.mat'), 'Beta');
fw_curve = F.GaussDiff(xe, Beta(1), Beta(2), 0);
end