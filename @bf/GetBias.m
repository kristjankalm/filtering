function [B, A, dfun, p, D] = GetBias(x, y, func, R)
% B -- curve params
%       dog -- b(1) amplitude, b(2) width
%       vm  -- b(1) kappa_q
% A -- amplitude of the curve
% dfun -- curve function
% p -- probability of A from null-dist
% D -- null distribution of bias amp values

if nargin < 3, func = 'dog'; end
if nargin < 4, n = 1; end

F = bf.GetFunctions;

switch func
    case 'vm'
        % error as a function of dy, error = y - posterior mean
        % F.VonMisesPosterior(x, kappa_R, kappa_Q);
        dfun = @(x, b) F.VonMisesPosterior(x, R, b);
        afun = @(k) F.VonMisesPosterior(F.VMP_Diff_0(k(1), R), k(1), R);
        % kappa_Q
        B = 0;
    case 'dog'
        % gaussian 1st der
        % F.GaussDiff(x, amplitude, width, intercept)
        dfun = @(x, b) F.GaussDiff(x, b(1), b(2), b(3));
        afun = @(b) b(1);
        % amplitude, width, intercept
        B = [0 1 0]; % init params
    otherwise
        error('curve func not specified!')
end

% Ordinary Least Squares cost function
ols = @(b) sum((dfun(x,b) - y).^2);
% fitting options
opts = optimset('MaxFunEvals', 10^8, 'MaxIter', 10^8);
% do fit
B = fminsearch(ols, B, opts);
A = afun(B);

% do probability distribution
% shuffle the data labels (relative orientation of the previous trial)
% prob = proportion of simulated values => A
D = []; % null distribution of amplitude values
p = [];

if nargout > 3
    
    % parallel pool
    %Opts.Pool = 1; 
    %Opts.Workers = 24;
    %par_open(Opts);
    
    num_shuffle = 10^3;
    num_rows = size(x,1);
    D = nan(num_shuffle,1);
    a_thresh = pi/4;
    
    % create random permutations of x    
    I = nan(num_shuffle, num_rows);
    rng('shuffle');
    for i = 1:num_shuffle
        I(i,:) = randperm(num_rows);
    end
    X = x(I);
    
    tic
    %parfor i = 1:num_shuffle
    parfor i = 1:num_shuffle
        xi = X(i,:)';
        olsr = @(b) sum((dfun(xi,b) - y).^2);
        bb = fminsearch(olsr, B, optimset('MaxFunEvals', 10^5, 'MaxIter', 10^5));
        % reject completely unrealistic fits
        a = afun(bb);
        if a < a_thresh && a  > -a_thresh
            D(i,1) = a;
        end            
    end
    toc
    D = D(~isnan(D));
    p = sum(D>=A) ./ size(D,1);
    
    %hist(D, linspace(-a_thresh, a_thresh, 10^2));
    
end

end