function [] = gca_rad2ang(gca, axis_label, round_to_decimal_places)
%GCA_RAD2ANG Summary of this function goes here
%   Detailed explanation goes here
a = upper(axis_label);
if nargin < 3, round_to_decimal_places = 0; end
dp = round_to_decimal_places;

% get y tics
tick_rad = get(gca, [a 'TickLabel']);
try
    if iscell(tick_rad)
        tick_rad = cellfun(@(x) str2num(strtrim(x)), tick_rad);
    else
        tick_rad = str2num(tick_rad);
    end
catch
    error('Label not convertible to numbers');
end

ytick_ang = round(circ_rad2ang(tick_rad), dp);
set(gca, [a 'TickLabel'], {ytick_ang'});

% y label
L = get(gca, [a 'Label']);
if contains(L.String, 'radian')
    L.String = replace(L.String, 'radian', 'degree');
end
set(gca, [a 'Label'], L);

end

