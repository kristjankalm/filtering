function w = GenSequence(s, l, k, P)
%GenSequence returns k sequence of length l which are as close as possible
%to sequence s
%   w output sequences
%   s input sequence
%   l desired length of w
%   k number of w
%   P permutation matrix of l-item sequences

%if nargin < 2, error('specify input sequence and desired len'); end
if nargin < 1, s = [1 2 3 4 5]; end
if nargin < 2, l = 5; end
if nargin < 3, k = 10; end
if nargin < 4, P = perms(1:l); end

if k > size(P,1), k = size(P,1)-1 ; end 

assert(isnumeric(s));
assert(isint(l) && isint(k));

for i = 1:size(P,1)
    L(i,1) = levenshtein(s, P(i,:));
end

[Ll, idx] = sort(L);

w = P(idx(1:k,1),:);
%w = arrayfun(@(x) {w(x,:)}, 1:size(w,1))';

end