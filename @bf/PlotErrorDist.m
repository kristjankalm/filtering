function kappa = PlotErrorDist(this, e, Opts)

if nargin < 3, Opts = []; end
if ~isfield(Opts, 'Bar'), Opts.Bar = 1; end
if ~isfield(Opts, 'FW'), Opts.FW = 0; end
if ~isfield(Opts, 'Color'), Opts.Color = [0 0 0]; end

%% plot params
LineWidth = 3;
FontSize = 18;
cols = brewermap(12,'Set1');
set(gcf, 'Color', [1 1 1]);
hold on

%% plot FW avg error dist
if Opts.FW
    % first plot FW
    FileFWStats = fullfile(this.Folder.Stats, '/stat_fw_kappa.mat');
    if exist(FileFWStats, 'file') == 2
        load(FileFWStats, 'kappa');
    else
        fw_stats();
    end
    kappa_fw = round(mean(kappa),2);
    x = linspace(-pi/4, pi/4, 10^2);
    % plot mean FW error dist
    line(x,  14* circ_vmpdf(x, 0, kappa_fw), 'Color', cols(1,:), 'LineWidth', 3);
end

%% plot model errors

% load and format errors
if nargin < 2 || isempty(e)
    this.Stats;
    load([this.Folder.Stats '/stat_' this.Label '_' this.Data '.mat'], 'E');
    e = E.e;
end
% convert errors to FW space
e = e./2;

x = linspace(-pi/4, pi/4, 10^2);
hy = histc(e, x);

if Opts.Bar
    Opts.Color = [0.2 0.2 0.2];
    Opts.BarWidth = 0.6;
    do_bar(hy, [], x, Opts);
end

set(gca, ...
    'YLim', [0 80], ...
    'XLim', [-pi/4 pi/4], ...
    'FontSize', FontSize);

%% fit a Von Mises distribution
vmfun = @(b, x) b(2) .* circ_vmpdf(x, 0, b(1));
B = nlinfit(x, hy, vmfun, [1 1]);
kappa = round(B(1),1);
line(x, vmfun(B, x), 'Color', Opts.Color, 'LineWidth', LineWidth);

%% disp

do_degrees = 1;

if Opts.FW
    % display std and kappa
    std_fw = sqrt(1/kappa_fw);
    std_m = sqrt(1/kappa);
    if do_degrees
        std_fw = num2str(round(circ_rad2ang(std_fw),2));
        std_m  = num2str(round(circ_rad2ang(std_m),2));
        k_fw = num2str(round(circ_rad2ang(kappa_fw),2));
        k_m = num2str(round(circ_rad2ang(kappa),2));        
    else
        std_fw = num2str(round(std_fw,2));
        std_m  = num2str(round(std_m,2));
        k_fw = num2str(round(kappa_fw,2));
        k_m = num2str(round(kappa,2));
    end
%     text_fw = ['std_E=' std_fw ', \kappa_E=' k_fw ];
%     text_m =  ['std_E=' std_m  ', \kappa_E=' k_m ];
    text_fw = ['std_E=' std_fw ];
    text_m =  ['std_E=' std_m  ];
    text(0.15, 50, text_fw, 'FontSize', FontSize, 'Color', cols(1,:));
    text(0.15, 59, text_m,  'FontSize', FontSize, 'Color', 'k');
end

xlabel('Error (radians)');
ylabel('Counts');
%title('Histogram of errors');

end
