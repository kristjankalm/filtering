function [ ] = vmf_bias_extrema( )
%VMF_BIAS_EXTREMA Summary of this function goes here
%   Detailed explanation goes here

F = bf.GetFunctions;

x = linspace(-pi, pi, 100);
k_1 = 22; k_2 = 1;
plot(x, F.VonMisesPosterior(x, k_1, k_2))

syms x k_1 k_2

df = diff(F.VonMisesPosterior, x);
df = simplify(df);
 
% df = (k_2*(k_2 + k_1*cos(x))) ... 
%       / ... 
%    (k_1^2 + 2*cos(x)*k_1*k_2 + k_2^2)

% solve for df == 0

[solx, params, conds] = solve(df, x, 'ReturnConditions', true);

end

