function g = DoParticleNoise(noisetype, sigma, numtrials, numparticles)
% sigma -- std (or it's analogue) of noise
% for circular data / von mises dist, the std is tranformed to kappa
% kappa = 1/(std^2)
disp('Pre-generating circular particle noise since noise is independent and 0-centred ...')

switch noisetype
    case {'c' 'circular'}

        sigma = circ_ang2rad(sigma); % from ang to radians
        kappa = 1/(sigma^2); % std -> kappa
        g = nan(numparticles,numtrials);
        tic;
        parfor t = 1:numtrials
            g(:,t) = circ_vmrnd(0, kappa, numparticles);
        end
        % since circ_vmrnd outputs 0 to 2*pi
        % map values to -pi to pi
        g = wrapToPi(g);
        toc;
    case {'n' 'normal'} % continuous particle noise
        g = normrnd(0, gs, l, numtrials);
    case {'d' 'poisson'} % discrete particle noise
        % sample possion values which determine
        % how much the particle moves in the sample space
        g = round(random('poiss', gs, l, numtrials));
        %gx = g(:,t) > 0; % index of move > 0
        %g(gx,t) = g(gx,t) + 1; % add 1 to the moves (why?)
        % choose exemplars g distance away
        %         for j = 1:l % for each particle
        %             if g(j)
        %                 %% THIS NOS NOT CURRENTLY WORK
        %                 %  try
        %                 %       vv = find(S(y(t),:) == g(j));
        %                 %       h(j,t) = randsample(vv,1);
        %                 %  catch
        %                 %       h(j,t) = z(j,t);
        %                 %  end
        %                 h(j,t) = z(j,t);
        %             else
        %                 h(j,t) = z(j,t);
        %             end
        %         end
        %         z(:,t+1) = h(:,t);
    otherwise
        error('Undefined particle noise profile!')
end

end