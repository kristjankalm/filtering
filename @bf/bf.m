classdef bf
    % single run of filtering experiment
    
    properties
        Exp
        Model
        Data
        Q % system noise
        R % likelihood / measurement noise
        H % measurement model
        A % system model
        FilterDist
        FuncMean
        FuncStd
        np % number of particles / points on grid
        beta % proportion to resample
        Label % Model & beta    
        Folder
    end
    
    methods
        function this = bf(varargin)

            default_A = @(x) x;   % default system model
            default_H = @(x) x;   % default measurement model
            default_np = 10^3;    % default num of particles, 10^3
            default_beta = 1;     % by default resample all
            default_Exp = 'rb';   % default exp 'rb': recency bias
            default_Model = 'vm'; % default model 'vm': von MIses
            default_Data = 'ur';
            
            isfunh = @(x) isa(x,'function_handle');
            
            % parse and validate inputs
            p = inputParser;
            
            addParameter(p, 'R',     1,             @isnumeric);    % measurement noise
            addParameter(p, 'Q',     1,             @isnumeric);    % system noise            
            addParameter(p, 'A',     default_A,     isfunh);        % system model function
            addParameter(p, 'H',     default_H,     isfunh);        % measurement model function
            addParameter(p, 'np',    default_np,    @isnumeric);    % number of particles
            addParameter(p, 'beta',  default_beta,  @isnumeric);    % prop to resmaple
            addParameter(p, 'Exp',   default_Exp,   @ischar);
            addParameter(p, 'Model', default_Model, @ischar);
            addParameter(p, 'Data',  default_Data,  @ischar);       % data file
            
            parse(p, varargin{:});
            
            % copy validated inputs to class properties
            Params = fieldnames(p.Results);
            for j = 1:size(Params,1)
                Param = Params{j,1};
                this.(Param) = p.Results.(Param);
            end
            
            if this.beta == 1
                this.Label = this.Model;
            else
                k = 1/(this.Q.^2); % std -> kappa
                this.Label = [this.Model '_' num2str(this.beta) '_' num2str(round(k,0))];
            end
            
            % paths etc
            FolderRoot = fileparts(fileparts(mfilename('fullpath')));
            FolderHome = fullfile(FolderRoot, ['_' this.Exp]);
            this.Folder.Home   = FolderHome;
            this.Folder.Mat    = fullfile(FolderHome, 'mat');
            this.Folder.Data   = fullfile(this.Folder.Mat, 'data');
            this.Folder.Filter = fullfile(this.Folder.Mat, 'filter');
            this.Folder.Stats  = fullfile(this.Folder.Mat, 'stats');
            this.Folder.Fig    = fullfile(FolderHome, 'fig');
            
            % create folders
            Fn = fieldnames(this.Folder);
            for i = 1 : size(Fn,1)
                F = this.Folder.(Fn{i});
                if exist(F, 'dir') ~= 7
                    disp(['Creating ' F])
                    mkdir(F);
                end
            end
            
        end
        
        r = Filtering(this)
                
        h = PlotStepsH(this, steps) % horis plot for diagnostics
        
        h = PlotStepsC(this, steps) % circular plot
        
        h = PlotStepsV(this, steps) % vertical plot
                
        k = PlotErrorDist(this, e, Opts)

        k = PlotMNErrorDist(this, e, Opts) % error dist for multinomial
                
        h = PlotError(this)
        
        h = PlotBias(this, n)
        
        h = PlotNback(this, n)
        
        % stats
        [E, D] = Stats(this, Opts)
                
        % data
        GenerateData(this)
    end
    
    methods(Static)
                
        [B, p] = FitErrFunc(dY, E, dfun, p);
        
        [B, A, dfun, p, D] = GetBias(x, y, func, R)               
        
        [BinnedData, edges, binsize, y_bins] = GetBinData(y, M, NumBins, edges)
        
        % distribution and other functions
        F = GetFunctions
        
        [w, d] = GetSequence(s, Q);
        w = GenSequence(s, l, k, P);
        
    end
    
end

