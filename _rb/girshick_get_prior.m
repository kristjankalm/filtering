% read in image
clear all
FileGif = 'girshick/girshick_dist.gif';
D = imread(FileGif);
[~, i] = min(D,[], 1);
i = size(D,1) - i;

y = i ./ size(D,1);
alpha = 0.01;
y = y.*alpha;

x = linspace(0,180, size(D,2));
y = [y(2:end) y(1)]; % shift y so peaks at 0, pi/2, pi
save('mat/girshick.mat', 'x', 'y');

plot(x,y);
set(gca, 'YLim', [0 0.01]);