clear all
close all
load('mat/girshick.mat', 'x', 'y', 'b');

% convert x to 0-2pi
x = circ_ang2rad(x.*2);

vmfun = @(b, x) (...
    b(4) .* (...
    ( ...
    (   b(1) / (2*pi*besseli(0,b(2)))     ) ...
    * exp(b(2) * cos(x))...
    ) + ...
    (...
    (   (1-b(1)) / (2*pi*besseli(0,b(3)))     ) ...
    * exp(b(3) * cos(x - pi)) ...
    ) ...
    )...
    )';

do_curve_fit = 1;
if do_curve_fit
    %% fit curve
    ols = @(b) sum((vmfun(b,x) - y').^2);
    % fitting options
    opts = optimset('MaxFunEvals', 10^8, 'MaxIter', 10^6);
    param_start = rand(4,1);
    % do fit
    b = fminsearch(ols, param_start, opts);
end

%% plot
FolderFig = '/home/kk02/Docs/_pubs/06_learning/fig/girshick/';
LineWidth = 3;
FontSize = 20;

cols = brewermap(3, 'Dark2');
figure('Position', [100 700 850 400]); hold on
set(gcf, 'Color', [1 1 1]);
% girshick data
scatter(x, y, 30, 'filled');
% individual VM components
vm1 = circ_vmpdf(x, 0, b(2)).*b(1);
vm1 = vm1 ./ sum(vm1);
plot(x, vm1, 'LineWidth', LineWidth, 'LineStyle', ':');
vm2 = circ_vmpdf(x, pi, b(3)).*(1-b(1));
vm2 = vm2 ./ sum(vm2);
plot(x, vm2, 'LineWidth', LineWidth, 'LineStyle', ':');
% mixture
vmmix = vmfun(b, x);
plot(x, vmmix, 'LineWidth', LineWidth);

set(gca, 'XLim', [0 2*pi], 'FontSize', FontSize);
%set(gca, 'YLim', [0 0.01]);

hy = ylabel('Probability', 'FontSize', FontSize);
hx = xlabel('Orientation', 'FontSize', FontSize);

xtick = 0:30:180;
xtick_rad = round(circ_ang2rad(xtick*2),2);
%xtick_rad = [0 pi/4 pi/2 3*pi/4 pi] .*2;
%xtick = {'0' '\pi/4' '\pi/2' '\pi 3/4' '\pi'};
set(gca, 'XTick', xtick_rad);
set(gca, 'XTickLabel', xtick);

set(gca, 'YTickLabel', '');
text(-0.5, 0.01, '0.01', 'FontSize', FontSize)
text(-0.2, 0, '0', 'FontSize', FontSize)

hold off


%% export
export_fig(fullfile(FolderFig, 'girshick_prior'), '-pdf');
% save variables
%save('mat/girshick.mat', 'b', '-append');
close all

%% mixture of Grishick natural prior and previous posterior

figure('Position', [100 700 850 400]); hold on
set(gcf, 'Color', [1 1 1]);
set(gca, 'XTick', xtick_rad);
set(gca, 'XTickLabel', xtick);
set(gca, 'XLim', [0 2*pi], 'FontSize', FontSize);
hy = ylabel('Probability', 'FontSize', FontSize);
hx = xlabel('Orientation', 'FontSize', FontSize);

% plot natprior
np = vmfun(b, x);
np = np ./ sum(np);

plot(x, np, 'LineWidth', LineWidth, 'LineStyle', ':', 'Color', cols(1,:));

% plot posterior
pp = circ_vmpdf(x, 2, 4);
pp = pp ./ sum(pp);
plot(x, pp, 'LineWidth', LineWidth, 'LineStyle', ':', 'Color', cols(2,:));

% posterior prop
m(1) = 0.1;
% natprior props
m(2) = (b(1)/sum(b(1:2))) .* (1-m(1));
m(3) = (b(2)/sum(b(1:2))) .* (1-m(1));

npmix = vmmixpdf(x, m', [2 0 pi]', [4 b(3) b(4)]');
npmix = npmix ./ sum(npmix);

plot(x, npmix, 'LineWidth', LineWidth, 'Color', cols(3,:));
tx = 4;
text(tx, 0.05, ['\beta_1= ' num2str(m(1))], 'FontSize', FontSize, 'Color', cols(2,:));
%text(tx, 0.04, ['\beta_2= ' num2str(1-m(1))], 'FontSize', FontSize, 'Color', cols(1,:));


suffix = num2str(m(1));
suffix = strrep(suffix, '.', '_');
export_fig(fullfile(FolderFig, ['mix_post_' suffix ]), '-pdf');

