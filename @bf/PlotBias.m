function d = PlotBias(this, n)
%PLOTBIAS Summary of this function goes here
%   Detailed explanation goes here
% n -- how many trials back

if nargin < 2, n = 1; end % default is 1-back recency bias

do_stats = 0;
if do_stats
    Opts.PreviousN = n;
    [E, D] = this.Stats(Opts);
else
    load([this.Folder.Stats '/stat_' this.Label '_' this.Data '.mat'], 'E', 'D');
end

d = D{n};
ylim = pi/3;

set(gcf, 'Color', [1 1 1]);
cols = brewermap(12,'Set1');
LineWidth = 2;
LineStyle = '-';
FontSize = 18;
if ~strcmp(this.Model, 'fw')
    k = [0.4 0.4 0.4];
else
    k = cols(1,:);
end

LineOpts = struct('LineWidth', LineWidth, 'LineStyle', LineStyle);
F = bf.GetFunctions;

%% plot binned errors and errorbars
%d.b = binned error datapoints
%d.m = binned error means, s - stds
increment = diff(d.edges(1:2)) / 2; % the shift to the mean of the bin on the plot
xe = d.edges - increment;

for i = 1 : size(d.b, 2)
    e_edges = xe(i); % edges are right sides of the bin
    e_edges = e_edges(ones( size(d.b{i},1) ,1),:); % expand to row vector
    scatter(e_edges, d.b{i}, 20, k), hold on;
end

if strcmp(this.Model, 'fw')
    h = errorbar(xe, d.m, d.s, 'Color', 'k');
end

%% plot the Von Mises fit
%R = d.R;
%Q = d.vm(1); % vm(1) k_1 or Q vm(2) k_2 or R
xx = linspace(-pi, pi, 10^2);
%vm_curve = d.vmfun(xx, d.vm);
%plot(xx, vm_curve, 'Color', cols(2,:), LineOpts);

%% plot the FW fit
% fisher and whitney average DOG parameters
Beta = [0.167, 1.0216, 0.0429]; % FW subjects average DOG params, stored at 'stat_fw_dog.mat'

xe = xx;
if ~strcmp(this.Model, 'fw')
    %load(fullfile(this.Folder.Stats, 'stat_fw_dog.mat'), 'Beta');
    fw_max = circ_ang2rad(20*2); % this is where the max line should be
    fw_curve = F.GaussDiff(xe, Beta(1), Beta(2), 0);
    plot(xe, fw_curve, 'Color', cols(1,:), LineOpts);
end

%% plot the DOG fit to data
dog_curve = F.GaussDiff(xe, d.dog(1), d.dog(2), 0);
plot(xe, dog_curve, 'Color', k, LineOpts);

%% vertical lines
if ~strcmp(this.Model, 'fw')
    % 1/2 pi lines
    %line([-ylim -ylim], [-ylim ylim], 'Color', 'r', 'LineStyle', ':')
    %line([ylim ylim], [-ylim ylim], 'Color', 'r', 'LineStyle', ':')
    %
    % % min max lines
    % FW max line
    line([fw_max fw_max], [-ylim ylim], 'Color', cols(1,:), 'LineStyle', ':', 'LineWidth', 2);
    
    % DOG max on X axis
    mc = xe(dog_curve == max(dog_curve(:)));
    line([mc mc], [-ylim ylim], 'Color', 'k', 'LineStyle', ':', 'LineWidth', 2);
    % VM max on X axis
    mvc = pi - acos(this.Q/this.R);
    %line([mvc mvc], [-ylim ylim], 'Color', cols(2,:), LineOpts)
end

%% axes
set(gca, 'XLim', [-pi pi]);
set(gca, 'YLim', [-ylim ylim]);
set(gca, 'FontSize', FontSize);

% convert axes values to fw space
set(gca, 'XTickLabels', get(gca, 'XTick')./2);
set(gca, 'YTickLabels', get(gca, 'YTick')./2);

%% text & labels
do_text = 0;
if do_text
    % vmfit info
    txt = ttxt(E.kappa, d.dog(1));
    
    if isfield(d, 'vm_p')
        p_str = num2str(round(d.vm_p,3));
        txt = [txt ', p_{VM} = ' p_str];
    end
    
    % dogfit info
    if isfield(d, 'dog_p')
        p_str = num2str(round(d.dog_p,3));
        txt = [txt ', p_{DOG} = ' p_str];
    end
    
    % FW params
    %fw_rss = sum(circ_dist(fw_curve, d.m).^2);
    %kappa_fw = 12.075; % mean posterior kappa for FW subjects
    %fw_txt = [ttxt(kappa_fw, Beta(1)), ', RSS_{FW} = ' num2str(round(fw_rss,4))];
    
    
    text(min(xe), -ylim - 0.05, txt);
    text(min(xe), -ylim - 0.15, fw_txt);
end

%ylabel('Error');
%xlabel(['y_{t-' num2str(n) '} - y_t']);
hy = ylabel('Error on current trial (radians)', 'FontSize', FontSize);
hx = xlabel('Relative orientation of previous trial (radians)', 'FontSize', FontSize);
% pos = get(hy,'pos'); % Read position [x y z]
% set(hy,'pos',pos+[.02 0 0]) % Move label to right

if ~strcmp(this.Model, 'fw') 
    do_degrees = 0;
    std_q = sqrt(1/this.Q); % kappa -> std
    std_q = std_q ./ 2;     % convert to FW space
    kappa_q = 1 ./(std_q.^2); % std -> kappa
    if do_degrees
        kappa_q = circ_rad2ang(kappa_q);
    end
    title_text = ['\kappa_Q = ' num2str(round(kappa_q,2))];
    if ~strcmp(this.Model, 'vm')
        title_text = [title_text ', \beta = ' num2str(this.beta)];
    end
    title(title_text, 'FontWeight', 'normal', 'FontSize', FontSize);
end

end

function txt = ttxt(k, a)

kappa_str = num2str(round(k,2));
std_str = num2str(round(sqrt(1/k),2));
a_str = num2str(round(a,2));
a_str_circ = num2str(round(circ_rad2ang(a),2));

txt = ['\kappa_E = ' kappa_str ' (' std_str ' \sigma), ' ...
    '\alpha_{DOG} = ' a_str ' (' a_str_circ '\circ)' ];
end

