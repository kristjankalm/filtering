function [] = fw_stats()
close all
%FW_STATS Summary of this function goes here
%   Detailed explanation goes here

Model = 'fw';
R = 0.133; % Fisher & Whitney, JND = 5.39; circ_ang2rad((5.39*2)/sqrt(2))
NumSubj = 4;

do_nback = 1;
do_error_dist = 1;
do_bias = 1;
do_bin_bias = 0;

%% nback plot
if do_nback    
    do_stats = 1;
    if do_stats
        NumSubj = 4;
        % get nback stats
        StatOpts.PreviousN = 3;
        for s = 1:NumSubj
            Data = [Model '_s' num2str(s)];
            ef = bf('Model', Model, 'Data', Data);          
            [ee{s}, d{s}] = ef.Stats(StatOpts);
            yy(s,:) = arrayfun(@(x) d{1,s}{1,x}.dog_a, 1:3);
        end
        
        yn = mean(yy([1 2 4],:)) ./ 2;
        yn_std = [0.02 0.02 0.02];
        save([ef.Folder.Stats '/stat_fw_nback.mat'], 'yn', 'yn_std');        
    else
        ef = bf('Model', Model);
        load([ef.Folder.Stats '/stat_fw_nback.mat'], 'yn', 'yn_std');
    end
    
    % % PLOT
    h = figure('Position', [50 100 300 500]);
    set(gcf, 'Color', [1 1 1]);
    FontSize = 16;
    Opts.BarWidth = 0.5;
    Opts.Alpha = 0;
    Opts.Color = [0.4 0.4 0.4];

    x = 1:3;
    
    do_bar(yn, yn_std, x, Opts);
    
    YLim = [-0.01 0.12];
  
    set(gca, ...
        'Box','off', ...
        'YLim', YLim);
    set(gca, 'FontSize', FontSize);
    
    %hy = ylabel('Amplitude of serial dependence (degrees)', 'FontSize', FontSize);
    hy = ylabel('Bias amplitude (radians)', 'FontSize', FontSize);    
    hx = xlabel('n-th trial back', 'FontSize', FontSize);
    
    % convert y-axis to degrees
    do_degrees = 1;
    if do_degrees
        gca_rad2ang(gca, 'y', 1);
    end
    
    FileName = fullfile(ef.Folder.Fig, ['nback_' ef.Model]);
    export_fig(FileName, '-pdf');
    
end

%% error distributions
if do_error_dist
    
    % [between-rows between-cols] [bottom-margin top-margin] [left-margin right-margin]
    subplot = @(m,n,p) subtightplot (m, n, p, [0.12 0.08], [0.1 0.03], [0.06 0.01]);
    
    h = figure('Position', [100 100 1000 600]);
    hold on
    
    for s = 1:NumSubj
        Data = [Model '_s' num2str(s)];
        ef = bf('Model', Model, 'Data', Data);
        load([ef.Folder.Data '/data_' Data '.mat'], 'y', 'r');
        e = circ_dist(r,y); % errors
        subplot(2,2,s);
        kappa(s) = ef.PlotErrorDist(e);
    end
    hold off
    export_fig(fullfile(ef.Folder.Fig, Model, 'errordist'), '-pdf');
    save([ef.Folder.Stats '/stat_fw_kappa.mat'], 'kappa'); 
end

%% recency bias
if do_bias
    do_degrees = 1;
    for s = 1:NumSubj
        Data = [Model '_s' num2str(s)];
        ef = bf('Model', Model, 'Data', Data, 'R', R);
        load([ef.Folder.Data '/data_' Data '.mat'], 'y', 'r');
        
        h = figure('Position', [100 100 650 500]);
        hold on
        d{s} = ef.PlotBias;
        
        % convert y-axis to degrees
        if do_degrees
            gca_rad2ang(gca, 'y');
            gca_rad2ang(gca, 'x');
        end
        
        hold off
        export_fig(fullfile(ef.Folder.Fig, Model, ['bias_' num2str(s)]), '-pdf');

    end
    % mean dog parameters
    Beta = cell2mat(arrayfun(@(x) d{x}.dog', 1:NumSubj, 'UniformOutput', false));
    Beta = mean(Beta,2);
    save(fullfile(ef.Folder.Stats, 'stat_fw_dog.mat'), 'Beta'); 
end

%% binned recency bias
if do_bin_bias
    
    NumBins = 13; % num of bins to split data
    n = 1;
    % [between-rows between-cols] [bottom-margin top-margin] [left-margin right-margin]
    subplot = @(m,n,p) subtightplot (m, n, p, [0.12 0.08], [0.1 0.05], [0.06 0.01]);
    
    %par_open(struct('Pool', 1));
    tic
    % preallocate data matrices
    A = nan(NumSubj, NumBins); % amplitude of DOG
    W = nan(NumSubj, NumBins); % width of DOG
    p = nan(NumSubj, NumBins);
    
    do_sb_plot = 1;
    
    for s = 1:NumSubj
        
        Data = [Model '_s' num2str(s)];
        ef = bf('Model', Model, 'Data', Data, 'R', R);
        load([ef.Folder.Data '/data_' Data '.mat'], 'y', 'r');
        
        e = circ_dist(r,y); % errors
        yy = y(n+1:end);
        e = e(n+1:end);
        dy = circ_dist(y(1:end-n), yy); % delta y
        % [BinnedData, edges, y_bins, binsize] = GetBinData(y, M, NumBins, edges)
        binsize = max(y)/NumBins;
        edges = linspace(binsize,max(yy), NumBins);
        [~, ~, yy_bins] = ef.GetBinData(yy, yy, NumBins, edges);
        
        if do_sb_plot
            h = figure('Position', [100 100 900 400]);
        end
        
        for j = 1:NumBins
            % filter certain stim values
            i = yy_bins == j;
            % get bias measure
            [b, a, dogfun] = ef.GetBias(dy(i), e(i), 'dog');
            %[b, dogfun] = GetBias(dy(i), e(i), 'dog');
            A(s,j) = b(1); % amplitude of DOG
            W(s,j) = b(2); % width of DOG
            H(s,j) = b(3); % intercept DOG
            
            % plot
            if do_sb_plot
                subplot(2,4,j)
                BiasPlot(b, dogfun, dy(i), e(i));
                xt = get(gca, 'XTick');
                yt = get(gca, 'YTick');
                set(gca, 'YTickLabel', round(circ_rad2ang(yt/2)));
                set(gca, 'XTickLabel', round(circ_rad2ang(xt/2)));
                text(-2, 1, ['\alpha = ' num2str(round(circ_rad2ang(A(s,j)),1))]);
                %text(0, 1, ['p = ' num2str(round(p(s,j),3))]);
                text(-2, -1, ['h = ' num2str(round(circ_rad2ang(H(s,j)),1))]);
                title(num2str(EdgeLabels(j)));
            end
        end
        
        if do_sb_plot
            export_fig([ef.Folder.Fig '/fw/s' num2str(s)], '-pdf');
        end
        
    end
    %save([ef.FolderData '/stat_fw.mat'], 'A', 'H', 'W', 'p');
end
end

function BiasPlot(B, dfun, dy, e)

% figure
LineWidth = 2;
FontSize = 10;
set(gcf, 'Color', [1 1 1]);

dyp = linspace(-pi,pi,100);

% [B, vmfun, p] = GetBias(dy, e, 'vm');
% plot(dyp, vmfun(B, dyp), 'LineWidth', LineWidth);


plot(dyp, dfun(dyp, B), 'LineWidth', LineWidth);
hold on

% plot errors
plot(dy, e, '.');

set (gca, ...
    'FontSize', FontSize, ...
    'XLim', [-pi pi], ...
    'YLim', [-pi/2 pi/2]);
hold off
end