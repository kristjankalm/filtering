function F = GetFunctions

% product of 2 von mises, taken from
% Murray, R. F., & Morgenstern, Y. (2010). Cue combination on the circle and 
% the sphere. Journal of Vision, 10(11), 15. http://doi.org/10.1167/10.11.15
F.VonMisesProductMu = @(x, m_1, m_2, k_1, k_2) m_1 + atan2( -sin(m_1 - m_2), ( (k_1/k_2) + cos(m_1 - m_2) ));
%µ1+ atan2(-sin(µ1-µ2),?1?2+ cos(µ1-µ2))

% k_1=kappa_Q, k_2=kappa_R
F.VonMisesPosterior = @(x, k_1, k_2) atan(k_2 .* sin(x) ./ (k_2 .* cos(x) + k_1));
%F.VonMisesPosterior = @(b, x) x - wrapToPi(atan2(b(2) .* sin(x), b(2) .* cos(x) + b(1)));

% first derivative of the VM Posterior function
F.VMP_Diff = @(x, k_1, k_2) ...
    (( k_1.^2 .* sin(x).^2 ) + ( k_1 .* (k_1 .* cos(x) + k_2) .* cos(x) )) ...
    ./ ...
    ( (k_1.^2 .* sin(x).^2) + (k_1 .* cos(x) + k_2).^2 ) ...
    ;
% VMP 1st deriv = 0 (to find max value of VMP)
F.VMP_Diff_0 = @(k_1, k_2) pi - acos(k_1/k_2);


% first derivative of Gaussian
% a     amplitude
% w     width
% c     intercept
F.GaussDiff = @(x, a, w, c) x .* a .* w .* (sqrt(2)./exp(-0.5)) .*exp(-(w.*x).^2) + c;


% std_q = (sqrt(1/this.Q) ./ 2);
% kappa_q = 1/(std_q.^2);

F.Std2Kappa = @(sd) 1./(sd.^2);
F.Kappa2Std = @(kappa) (sqrt(1./kappa) ./ 2);

% c_kappa = @(kappa_1, kappa_2) besselratioInverse(0, besselratio(0, kappa_1)*besselratio(0, kappa_2) ,'sraamos');
% % Von Mises fit
% vmfun = @(b, x) x - wrapToPi(atan2(this.R * sin(x), this.R * cos(x) + b(1)));
% % DOG fit
% dogfun = @(b, x) x .* b(1) .* b(2) .* (sqrt(2)/exp(-0.5)) .*exp(-(b(2).*x).^2);

end
