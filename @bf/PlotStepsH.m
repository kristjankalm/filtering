function h = PlotStepsH(this, steps)

if isscalar(steps)
    steps = 1:steps;
end

load(fullfile(this.Folder.Filter, [this.Label '_' this.Data '.mat']), ...
    'M', 'P', 'r', 'M_mu', 'M_std', 'P_mu', 'P_std');
load(fullfile(this.Folder.Data, ['data_' this.Data '.mat']), 'y');

%% horisontal distribution

num_subplots = size(steps,2);
FontSize = 16;

figure('Position', [100, 100, 600, num_subplots*90], 'Renderer','opengl');
% [between-rows between-cols] [bottom-margin top-margin] [left-margin right-margin]
subplot = @(m,n,p) subtightplot (m, n, p, [0.03 0.05], [0.01 0.01], [0.01 0.01]);
set(gcf, 'Color', [1 1 1]);
cols = brewermap(12,'Set1');

do_normalise = 1; % scale all curves btw 0 and 1

x = linspace(0, 2*pi, this.np);

for j = 1:num_subplots
    s = steps(j);
    h{j} = subplot(num_subplots, 1, j); hold on;
    
    % stimulus
    %%line([y(s) y(s)], [0 ylim(2)], 'LineStyle', ':', 'Color', cols(2,:), 'LineWidth', 2);
    yVM = VMDistribution(y(s), this.R);
    yl = plotP(yVM, x, cols(2,:));
    
    % predictive prior
    plotP(P{s}, x, cols(1,:));
    %ylim = get(gca, 'YLim');
    %line([P_mu(s) P_mu(s)], [0 ylim(2)*0.8], 'LineStyle', '-', 'Color', cols(1,:), 'LineWidth', 1);
    
    % posterior
    ym = plotP(M{s}, x, cols(3,:));
    % posterior mean
    %line([M_mu(s) M_mu(s)], [0 ylim(2)*0.8], 'LineStyle', '-', 'Color', cols(3,:), 'LineWidth', 1);
    
    % response
    %scatter(r(s), 0, 30, cols(3,:), 'filled');
    M_max = x(ym == max(ym));
    %line([M_max M_max], [0 max(ym)], 'LineStyle', ':', 'Color', cols(3,:), 'LineWidth', 2);
    % stimulus dot
    scatter(y(s), 0, 30, cols(2,:), 'filled');
    % stim line
    line([y(s) y(s)], [0 max(yl)], 'LineStyle', ':', 'Color', cols(2,:), 'LineWidth', 2);
    
    % init distribution
    if s == 1
        %tP(this.FilterDist(0, this.R), [0 0 0]);
    end
    
    set(gca, ...
        'XLim', [0 2*pi], ...       
        'visible', 'off');
    ylim = get(gca, 'YLim');
    text(0.05, ylim(2)/2, ['t=' num2str(s)], 'FontSize', FontSize);
    
    hold off
end
end

function plotH(P, x, Colour)
LineWidth = 2;
LineStyle = '-';
Args = {'Color', Colour, 'LineWidth', LineWidth, 'LineStyle', LineStyle};

y = histc(P.d, x);
y = y ./ sum(y(:)); % normalise sum = 1
%y = y ./ 5;
plot(x, y, Args{:});

end

function y = plotP(P, x, Colour)

LineWidth = 2;
LineStyle = '-';

Args = {'Color', Colour, 'LineWidth', LineWidth, 'LineStyle', LineStyle};

switch class(P)
    case 'GaussianDistribution'
        error('not done yet')
    case 'VMDistribution'
        mu = P.mu;
        std = P.std;
        kappa = P.kappa;
        %l = P.plot();
        y = circ_vmpdf(x, mu, kappa);
        %text(mu, 0.1, num2str(round(kappa,1)), 'Color', Colour, 'FontSize', 8);        
    case 'WDDistribution'
        x = P.d;
        [x, srt] = sort(x);
        y = P.w(srt);        
    otherwise
        error('not done yet')
end

y = y ./ sum(y(:)); % normalise sum = 1
plot(x, y, Args{:});

% show std values
%text(mu, YMax*0.9, num2str(round(kappa,1)), 'Color', Colour, 'FontSize', 8);

% errorbar
%herrorbar(mu, max(l.YData), std, std, 'Color', Colour);
end