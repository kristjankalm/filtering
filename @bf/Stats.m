function [E, D] = Stats(this, Opts)
% y - stimuli
% r - estimates / responses
if nargin < 1, error('No model spec!'); end
Model = this.Model;
Data  = this.Data;

if contains(Model, 'kf') % gaussian
    Opts.FunMean = @mean;
    Opts.FunStd  = @std;
    Opts.FunDist = @minus;    
else % von Mises
    Opts.FunMean = @circ_mean;
    Opts.FunStd  = @circ_std;
    Opts.FunDist = @circ_dist;
end

if ~isfield(Opts, 'PreviousN')
    Opts.PreviousN = 1; % calculate recency fx N trials back
end
if ~isfield(Opts, 'NumBins')
    Opts.NumBins = 13; % number of bins in error analysis
end

% load stim values
load([this.Folder.Data '/data_' Data '.mat'], 'y');

% % load responses
switch Model
    case 'fw'
        load([this.Folder.Data '/data_' Data '.mat'], 'r');
    otherwise
        load([this.Folder.Filter '/' this.Label '_' Data '.mat'], 'r');
        %load([this.Folder.Filter '/' this.Label '_' Data '.mat'], 'M_mu');
        %r = M_mu';
end

%% stats

% errors
Er = Opts.FunDist(r, y); % response minus y
% fit VM distr to errors
x = linspace(-pi/2, pi/2, 10^2);
vmfun = @(b, x) b(2) .* circ_vmpdf(x, 0, b(1));
hy = histc(Er, x);
B = nlinfit(x, hy, vmfun, [1 1]);
E.kappa = B(1);

%% Bin errors
% organise single subject data into stimulus bins
binsize = 2*pi/Opts.NumBins;
edges = linspace(binsize,2*pi, Opts.NumBins); % edges are right sides of the bins
E.eb = bf.GetBinData(y, Er, Opts.NumBins, edges);
% calculate circular stats
% Er_m = cellfun(@circ_mean, MBins);
% Er_s = cellfun(@circ_std, MBins);
E.m = cellfun(@Opts.FunMean, E.eb);
E.s = cellfun(@Opts.FunStd, E.eb);
E.e = Er;
E.edges = edges;

% Bin responses
E.r = bf.GetBinData(y, r, Opts.NumBins, edges);
E.rm = cellfun(@Opts.FunMean, E.r);
E.rs = cellfun(@Opts.FunStd, E.r);

% % estimate parameters of error distribution
% % distribution of errors -- are not Gaussian since angular distance is a bounded measure
% % using von Mises

% dY        delta_y = y_{t-1} minus y_t
% D.y       binned data of dY
% D.edges   edges of bins
% D.m       means of binned data
% D.s       stds of binned data
% D.p       probability of amp from 0-distribution

% edges for distance bins
dy_edges = linspace(-pi+binsize,pi, Opts.NumBins);

for n = 1 : Opts.PreviousN
    disp([num2str(n) '-back ...']);
    % difference btw prev & current trials
    % delta_y = y_{t-1} minus y_t
    D{n}.dy = Opts.FunDist(y(1:end-n), y(n+1:end));
    % error on trial
    D{n}.e = Er;
    % bin data
    [D{n}.b, D{n}.edges] = bf.GetBinData(D{n}.dy, Er(n+1:end), Opts.NumBins, dy_edges);
    % calculate mean and std for every bin
    D{n}.m = cellfun(@Opts.FunMean, D{n}.b);
    D{n}.s = cellfun(@Opts.FunStd, D{n}.b);
    if size(y,1) > 10^2
        % fit 1st derivative of Gaussian
        [D{n}.dog, D{n}.dog_a, D{n}.dogfun, D{n}.p, D{n}.D] = ... 
            bf.GetBias(D{n}.dy, Er(n+1:end), 'dog', this.R);
        D{n}.dog_std = nanstd(D{n}.D);
        %D{n}.dog = bf.FitErrFunc(D{n}.dy, Er(n+1:end), dogfun);
        % fit Von Mises
        [D{n}.vm, D{n}.vm_a, D{n}.vmfun] = bf.GetBias(D{n}.dy, Er(n+1:end), 'vm', this.R);        
        %D{n}.kappa = bf.FitErrFunc(D{n}.dy, Er(n+1:end), vmfun);
        %D{n}.vmfun = vmfun;
    end
    D{n}.R = this.R;
    % residual error to DOG fit
    %D{n}.Rss_dog = sum(circ_dist(dog, Er(2:end)).^2);
end

%% get recency biases for stimulus bins
% [J.y, J.edges, ~, y_bins] = GetBinData(y, y, 12);

% save
save([this.Folder.Stats '/stat_' this.Label '_' Data '.mat'], 'E', 'D');

end
