function [BinnedData, edges, y_bins, binsize] = GetBinData(y, M, NumBins, edges)
% y -- source data, bins created accordingto values here
% M -- dependent data, to be binned

% assumes equal bin sise
binsize = diff(edges(1:2));

% edges are right sides of the bins
if nargin < 4
    binsize = max(y)/NumBins;
    edges = linspace(binsize,max(y), NumBins);    
end

% length(edges) = NunBins+1

% Y = discretize(X,edges) returns the indices of the bins that contain the elements of X. 
% The jth bin contains element X(i) if edges(j) <= X(i) < edges(j+1) for 1 <= j < N, 
% where N is the number of bins and length(edges) = N+1. 
% The last bin contains both edges such that edges(N) <= X(i) <= edges(N+1).

% [___] = discretize(___,'IncludedEdge',side), where side is 'left' or 'right', 
% specifies whether each bin includes its right or left bin edge. 
% For example, if side is 'right', then each bin includes the right bin edge, 
% except for the first bin which includes both edges. In this case, 
% the jth bin contains an element X(i) if edges(j) < X(i) <= edges(j+1), 
% where 1 < j <= N and N is the number of bins. The first bin includes 
% the left edge such that it contains edges(1) <= X(i) <= edges(2). 
% The default for side is 'left'.

disc_edges = [edges(1)-binsize edges];
y_bins = discretize(y, disc_edges, 'IncludedEdge','right');

BinnedData = arrayfun(@(b) M(y_bins == b), 1:NumBins, ...
    'UniformOutput', false);

% number of datapoints in bins
%ss = cell2mat(cellfun(@(x) size(x,1), MBins, 'UniformOutput', false))

% replace empty cells with NaNs
for b = 1:size(BinnedData,2)
    if size(BinnedData{b},1) == 0
        BinnedData{b} = NaN;
    end
end

end