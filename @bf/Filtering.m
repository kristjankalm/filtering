function r = Filtering(this)
% Model     str specifying filtering model
% beta      mixing coefficient parameter
tic

load([this.Folder.Data '/data_' this.Data '.mat'], 'y');

% kf    -- kalman filter
% ukf   -- circular unscented KF
% vmb   -- pure recursive von Mises, tracks mean
% vm    -- von mises filter
% vmbi  -- von mises filter with bimodal prior
% vmy   -- von mises filter, posterior mu = y
% vmpf  -- von mises particle filter
% vmpfn -- von mises particle filter, nat prior

% set noise distribution paramaters
StateNoise = this.FilterDist(0, this.Q);
MeasurementNoise = this.FilterDist(0, this.R);
np = this.np; % num of particles

switch this.Model
    case {'vm' 'vmy' 'demo' 'vmbi'}
        f = VMFilter();
    case 'k'
        f = KalmanFilter();
    case 'uk'
        f = CircularUKF();
    case {'vmp' 'vmpn'}
        f = CircularParticleFilter(np);
    case 'vmm'
        f = CircularMixtureFilter(np);
    case {'cd' 'cdm' 'cdu' 'cdmixdemo'} % circular discrete, circular discrete mixtures
        f = DiscreteFilter(np);
        % precalculate StateNoise distribution since it's static
        StateNoise_ = WDDistribution((0:np-1)/np*2*pi);
        StateNoise_ = WDDistribution(StateNoise_.d, ...
            arrayfun(@(x) StateNoise.pdf(x), StateNoise_.d));
        StateNoise = StateNoise_;
        this.A = @maxap;
    case 'torvm' % toroidal VM
        f = [];
    otherwise
        error('No specified model found!')
end

% is the Filter of mixture distribution type?
ismix = contains(class(f), 'MixtureFilter');
% is there a system transition function ?
isstf = ~isequal(func2str(this.A), '@(x)x');

% init latent var
switch this.Model
    case 'mn' % discrete multinomial
        % sample random np sequences of length y
        d = cell(1,np);
        for i = 1:np
            d{i} = randperm(size(y{1},2));
        end
        f.setState(MNDistribution(np, d)); % init particles > keep the initial MN
    otherwise
        f.setState(this.FilterDist(0, 1/10^5)); % mu, kappa; > almost uniform
end

for k = 1:size(y,1)
    
    %predict
    if isstf % system transition function
        f.predictGrid(this.A, StateNoise, this.beta); % natprior mixture
    else % (identity), arg -> distribution of additive noise
        f.predictIdentity(StateNoise);
    end
          
    P{k} = f.getEstimate;
    
    % update (identity), arg -> distr of measurement noise, observation y
    f.updateIdentity(MeasurementNoise, y(k));
    
    M{k} = f.getEstimate;
        
    % simulate a subject's response
    r(k,1) = f.getEstimate.sample(1);
    
    if ismix
        f.updateMixture(P{k}, this.beta);
    end
    
end

SaveMode = 'min'; % full, min, none
SaveMode = 'full';
FileName = [this.Label '_' this.Data '.mat'];
switch SaveMode
    case 'full'
        % convert prior and posterior estimates to matrix forms
        % predictive prior
        P_std = arrayfun(@(x) P{x}.(this.FuncStd), 1:size(y,1));
        P_mu  = arrayfun(@(x) P{x}.(this.FuncMean), 1:size(y,1));
        % posterior/ estimate
        M_std = arrayfun(@(x) M{x}.(this.FuncStd), 1:size(y,1));
        M_mu  = arrayfun(@(x) M{x}.(this.FuncMean), 1:size(y,1));
        % save
        save(fullfile(this.Folder.Filter, FileName), 'M', 'P', 'M_mu', 'M_std', 'P_mu', 'P_std', 'r');
    case 'min'
        save(fullfile(this.Folder.Filter, FileName), 'M', 'P', 'r');
    otherwise
end

toc
end
