function h = PlotStepsC(this, steps)
% circular plot
Model = this.Model;
Data  = this.Data;

if isscalar(steps)
    steps = 1:steps;
end

load([this.FolderData '/' Model '_' Data '.mat'], 'M', 'P');
load([this.FolderData '/data_' Data '.mat'], 'y');
%load([this.FolderData '/stat_' Model '_' Data '.mat'], 'E','D');

%% horisontal distribution

num_subplots = size(steps,2);

figure('Position', [100, 100, num_subplots*350, 350], 'Renderer','opengl');
% [between-rows between-cols] [bottom-margin top-margin] [left-margin right-margin]
subplot = @(m,n,p) subtightplot (m, n, p, [0 0], [0 0], [0 0]);
set(gcf, 'Color', [1 1 1]);

for j = 1:num_subplots
    s = steps(j);
    sb = subplot(1, num_subplots, j); hold on;
    % predictive prior
    VMDistribution.plotCircle('Color', [0.6 0.6 0.6]);
   
    % text
    if s > 1
        %Dy = round(D{1}.dy(s-1),3);
        %Er = round(D{1}.b{:}(s-1),3);
        Er = round(circ_dist(M{s}.mu, y(s)),3);
        text(-0.5, 0, ['[' num2str(s) '] e:' num2str(Er)], 'FontSize', 10);
    else
        text(0, 0, num2str(s), 'FontSize', 10);
    end
    
    P{s}.plot2dcirc(0.9, 'Color', [1 0 0], 'LineWidth', 2); 
    % posterior
    M{s}.plot2dcirc(0.95, 'Color', [0 0.5 0], 'LineWidth', 2);
    % measurement
    Y = this.FilterDist(y(s), this.R);
    Y.plot2dcirc(1.05, 'Color', [0 0 1], 'LineWidth', 2);
    % init distribution
    if s == 1
        I = this.FilterDist(0, this.R);
        I.plot2dcirc(0.85, 'Color', [0.4 0.4 0.4], 'LineWidth', 1);
    end
    set(sb, 'FontSize', 8);
    hold off
end
end

function plotP(P, Colour)
if isprop(P, 'd')
    P.ploth(80, 'FaceColor', Colour);
end
switch class(P)
    case {'GaussianDistribution', 'VMDistribution'}
        mu = P.mu;
        std = P.std;
        l = P.plot('Color', Colour);
    otherwise
        mu = P.circularMean;
        std = P.circularStd;
        kappa = 1/(std^2);
        l = VMDistribution(mu, kappa).plot('Color', Colour);
end
herrorbar(mu, max(l.YData), std, std, 'Color', Colour);
end


