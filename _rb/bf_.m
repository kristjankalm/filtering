function bf_
close all

Model = 'vm';   % von mises filter
%Model = 'cdm'; % circular discrete filter with mixture transition function: natural prior
%Model = 'vmm'; % von mises mixture filter
%Model = 'vmp'; % global mean model
%Data = 'fw_s1';
Data = 'gr';


R = 0.133; % in std; Fisher & Whitney, JND = 5.39; circ_ang2rad((5.39*2)/sqrt(2))

switch Model
    case 'cdm'
        Q = 0.19; % in std
        beta = 0.75;
    case 'vmm'
        Q = 0.21;
        beta = 0.75;  
    case 'vm'
        Q = 0.28;
        beta = 1;
    case 'vmp' % global mean
        Q = 0.2;
        beta = 1;        
    otherwise
        Q = 0.17; % in std; vm 0.17; cdm 0.16; vmm 0.21 
        beta = 1;
end

% init model params
ef = bf('Model', Model, 'Data', Data, 'R', R, 'Q', Q, 'np', 10^3, 'beta', beta);
% format data
ef.GenerateData;

ef.FilterDist = @VMDistribution; % noise type
ef.Q = 1/(ef.Q.^2); % std -> kappa
ef.R = 1/(ef.R.^2); % std -> kappa
ef.FuncMean = 'circularMean';
ef.FuncStd = 'circularStd';

disp(ef.Label)
n = 1; % recency bias N
 
do_filter = 1;
do_edist  = 1;
do_steps  = 1;
do_bias   = 1;
do_nback  = 1;

if do_filter
    ef.Filtering;
end

% === PLOTS ==
do_degrees = 1;
cols = brewermap(12,'Set1'); 
%% error distribution plot
if do_edist    
    h = figure('Position', [500 100 650 500]);           
    % plot model errors
    Opts.FW = 0;
    ef.PlotErrorDist([], Opts);
    if do_degrees
        gca_rad2ang(gca, 'x', 0);
    end
    FileName = fullfile(ef.Folder.Fig, ['ed_' ef.Model]);
    %export_fig(FileName, '-pdf');
end

%% step by step
if do_steps
    s = 1:6;
    ef.PlotStepsH(s);
    %export_fig(fullfile(ef.Folder.Fig, ['stepsH_1_' ef.Label]), '-pdf');
end

%% binned recency bias plot
if do_bias    % binned bias plot
    h = figure('Position', [50 100 650 500]);
    ef.PlotBias;
    if do_degrees
        gca_rad2ang(gca, 'x', 0);
        gca_rad2ang(gca, 'y', 0);
    end
    FileName = fullfile(ef.Folder.Fig, ['bias_' ef.Model]);
    %export_fig(FileName, '-pdf');
end

%% nback
if do_nback
    h = figure('Position', [50 100 300 500]);
    ef.PlotNback;    
    if do_degrees
        gca_rad2ang(gca, 'y', 1);
    end
    FileName = fullfile(ef.Folder.Fig, ['nback_' ef.Model]);
    %export_fig(FileName, '-pdf');
end

end
