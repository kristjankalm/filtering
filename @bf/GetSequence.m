function [w, d] = GetSequence(s, Q)
%GetSequence returns a sequence W which is d distance away from input
%sequence S
%   w output sequence
%   s input sequence
%   Q precision of Mallows

if nargin < 1, error('specify input sequence'); end
if nargin < 2, Q = 1; end

assert(isnumeric(s) && isnumeric(Q));

l = length(s);

%% human confusion matrix

% mallows dist
% x = 0:l-1;
x = [0 1 2];
p = exp(x.* -Q);
p = p ./ sum(p);
%d = discretesample(p, 1)-1;
d = datasample([0 2 4], 1, 'Weights', p, 'Replace', false);

w = s;

if d
    % confusion matrix q
    q = [0.0116 0.0316 0.0861 0.2341 0.6364];
    ql = length(q);
    
    try
        p = datasample(1:l, d, 'Weights', q(end-l+1:end), 'Replace', false);
    catch
        error
    end
    switch d
        case 1
            if l < ql
                w = [w ql]; % insertion
            else
                w = w(w ~= w(p)); % deletion
            end
        case 2
            % swap
            w = e_swap(w, p);
        case 3
            % swap
            w = e_swap(w, p(1:2));
            % delete
            w = w(w ~= w(p(3)));
        case {4,5}
            % 2 swaps
            w = e_swap(w, p(1:2));
            w = e_swap(w, p(3:4));
        otherwise
    end
    
end % if d


%% generate edits

% % prob for edits: insertions, deletions, substitutions
% edit_prob = [1/3 1/3 1/3];
%
% % position x edit probability
% lambda_p = 0.7;
% ls = length(s)+3;
% q = exp((0:ls).* -lambda_p);
% q = q ./ sum(q);
% q = fliplr(q);
% w = s;
%
% if d
%     for j = 1:d % for every edit
%         % edit which pos
%         try
%         p = discretesample(q(end-length(w)+1:end), 1);
%         catch
%             error
%         end
%         % sample edit: insertions, deletions, substitutions
%         e = discretesample(edit_prob, 1);
%         switch e
%             case 1
%                 % insertion
%                 item = randsample(l, 1); % sample new to-be-inserted item
%                 if p == 1
%                     w = [item w];
%                 else
%                     w = [w(1:p-1) item w(p:end)];
%                 end
%             case 2 % deletion
%                 if p == 1
%                     w = w(2:end);
%                 elseif p == l
%                     w = w(1:end-1);
%                 else
%                     w = [w(1:p-1) w(p+1:end)];
%                 end
%             case 3 % substitution > replace one item with another
%                 no_item = s(s ~= w(p));
%                 w(p) = randsample(no_item,1);
%             otherwise
%                 error('undefined')
%         end
%     end
%
% end % if d


end

function w = e_swap(w, p)
p1 = p(1);
p2 = p(2);
w(p(1)) = p2;
w(p(2)) = p1;
end
