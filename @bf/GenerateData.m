function GenerateData(this)
% generate data for the filtering simulations

n = 800*2; % how many datapoints / trials

switch this.Data
    case 'demo' % bayes data -- data for animation and pres
        r = 1.5;
        y = [circ_ang2rad(100) pi+(pi/3) 2*pi-1 1 pi+(pi/3) 1]';
        y = wrapTo2Pi(y);
    case 'bin' % binned data
        numbins = 26;
        r = 12; % repeats of single val
        n = numbins * r;
        y = linspace(0, 2*pi, numbins);
        y = kron(ones(1,r), y);
        y = randsample(y, n, false)';
    case 'grw' % gaussian random walk data
        std_q = 0.5;
        q = random('normal', 0, std_q, n, 1);
        y(1) = 0;
        for i = 2:n
            y(i,1) = y(i-1) + q(i);
        end
        %plot(y)
    case 'gr' % gaussian random
        std = 1;
        y = random('normal', 0, std, n, 1);
    case 'ur' % random data, uniform
        y = random('uniform', 0, 2*pi, n, 1);
        y = 2*pi/n : 2*pi/n : 2*pi;
        y = y(randperm(n))';
        %dy = circ_dist(y(1:end-1),y(2:end));
        %hist(y);
    case 'vmy' % intervals of distance
        y = [pi, linspace(0, 2*pi, 10)]';
    case 'vmp' % comparison on vm & vmp
        n = 10;
        % distance of 2 rad, where no rec bias for pf
        dy = normrnd(2, 0.1, n, 1);
        dy = dy .* randsample([-1 1], n, true)'; % assign random sign
        y = wrapTo2Pi(cumsum(dy));
        % distance of 0.5 rad, where clear rec bias for pf
        % dy = normrnd(0.6, 0.1, n, 1);
        % y = [y; wrapTo2Pi(cumsum(dy))];
    case 'vm2' % pair of opposing angles
        y = linspace(0,2*pi, n/2);
        y = [y; y+pi];
        y = reshape(y, 1, [])';
        y = wrapTo2Pi(y);
    case 'linreg'
        T = [0; 0.4]; % intercept, slope
        H = [ones(1,n); 1:n]';
        std = 1;
        e = random('normal', 0, std, n, 1);
        y = H*T + e;
    case 'rnd4'
        w = 4; % length of sequence
        % random sequences
        for i = 1:n
            y(i, :) = randperm(w);
        end
    case {'fw' 'fw_s1'}
        % data from Fisher & Whitney, 2015
        % Each participant has 8 runs, 4 of which were run
        % with the stimulus to the left of fixation
        % and the other four with the stimulus to the right.
        
        Subjects = 1:4;
        Position = {'left' 'right'}; % position of stimulus from fixation
        Run = 1:4;
        
        FileFolder = fullfile(this.Folder.Home, 'deidentified');
        for s = Subjects
            i = 1;
            y = [];
            for p = Position
                for j = Run
                    FileName = fullfile(FileFolder, ['S' num2str(s) '_' p{:} '_' num2str(j) '.txt']);
                    disp(FileName);
                    % result= dlmread(filename,delimiter,r,c,range)
                    y = [y; dlmread(FileName, '\t', 2, 0)]; % read data, skip first 2 rows
                    i = i + 1;
                end % run
            end % position
            
            %hist(y(:,1), [-90:2:90])
            % convert from angles to radians
            y = circ_ang2rad(y);
            % convert from [-pi/2 pi/2] -> 0 to 2*pi
            y = (y + pi/2)*2;
            
            r = y(:,2); % responses
            y = y(:,1); % stimuli
            
            % save mat
            FileName = fullfile(this.Folder.Data, ['data_fw_s' num2str(s) '.mat']);
            save(FileName, 'y', 'r');  
        end % subjects
       
    otherwise
        error('No valid data model specified!')
end

if ~exist('Subjects') % don't save if F&W
    save(fullfile(this.Folder.Data, ['data_' this.Data '.mat']), 'y');
end

end

