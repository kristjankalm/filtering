function y = vmmixpdf(x, p, mu, kappa)
% x     mixture evaluated at 
% parameters
% p     vector of mixing coefficients, nx1
% mu    vector of means, nx1
% kappa vector of precisions, nx1
% output
% y     output probabilities

if nargin < 4
    error('Not enough inputs')
end

assert(size(p,1) >= size(p,2), 'Parameters must be row vectors! Must: size(p,1) >= size(p,2) ');
    

if size(kappa,1) == 1
    kappa = kappa(ones(size(mu,1),1),1);
end

if size(p,1) ~= size(kappa,1) || size(p,1) ~= size(mu,1)
    error('Parameter vectors need to be of the same size')
end

if size(x,1) > 1
    error('Input x must be a column vector, 1xn')
end

% preload y
y = nan(size(p,1), size(x,2));

% evaluate pdf
for i = 1:size(p,1)
    C = p(i) / (2*pi*besseli(0,kappa(i)));
    y(i,:) = C * exp(kappa(i) * cos(x - mu(i)));
end

y = sum(y,1);

end