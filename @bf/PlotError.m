function h = PlotError(this)

% load data
%load([this.FolderData '/' Model '_' Data '.mat'], 'M', 'P');
load([this.FolderData '/data_' this.Data '.mat'], 'y');
load([this.FolderData '/stat_' this.Model '_' this.Data '.mat'], 'E','D');

amin = 0;
amax = 2*pi;

%% plot errors
h = figure('Position', [100 100 700 400]);
% binned errors (y minus response)
xbin = E.edges(2:end) - E.binsize;
Opts.Color = [0 0 0];
Opts.E = E.s; % std of error bin
Opts.XLabel = 'Stimulus value';
Opts.YLabel = 'Error';
do_ebar(E.m, xbin, Opts);
set(gca, 'XLim', [0 2*pi])

%% binned responses
% Opts.E = E.rs; % std of error bin
% Opts.Color = [0 0.8 0];
% do_ebar(E.rm, xbin, Opts);
% set(gca, 'XLim', [amin amax]);
% set(gca, 'YLim', [-amax amax]);
% line([amin amax], [0 0], 'Color', [0 0 0]);
% %export_fig(fullfile('fig', ['err_' Model]), '-pdf');


end
