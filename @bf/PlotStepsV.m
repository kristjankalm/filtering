function h = PlotStepsV(this, steps)
Model = this.Model;

if isscalar(steps)
    steps = 1:steps;
end

load([Model '_ur.mat'], 'M_mu', 'M_std', 'P_mu', 'P_std', 'R', 'Q');
load('data_ur.mat', 'y');

% convert params
if strfind(Model, 'vm') % von Mises
    S = 1/sqrt(R); % kappa -> std
else % gaussian    
    S = sqrt(R); % var -> std
end

%% vertical mean + std
xn = size(steps,2);

figure('Position', [100 100 xn*80 600])

% data
Opts.Color = [0 0 1];
Opts.E = ones(1,xn) * S;
do_ebar(y(steps)', steps, Opts);
hold on

% prior
Opts.Color = [1 0 0];
Opts.E = P_std(steps);
do_ebar(P_mu(steps), steps+0.1, Opts);

% estimates (posterior)
Opts.Color = [0 .7 0];
Opts.E = M_std(steps);
do_ebar(M_mu(steps), steps+0.2, Opts);

% init
Opts.Color = [0 0 0];
Opts.E = S; % var -> std
do_ebar(0, 0.5, Opts);

hold off

end


