function [a, s] = PlotNback(this, n)
%PLOTBIAS Summary of this function goes here
%   Detailed explanation goes here
% n -- how many trials back

if nargin < 2, n = 3; end % default is 1-back recency bias

do_stats = 1;
if do_stats
    Opts.PreviousN = n;
    [~, D] = this.Stats(Opts);
else
    load([this.Folder.Stats '/stat_' this.Label '_' this.Data '.mat'], 'D');
end

x = 1:3;
% alpha of DoG
a = arrayfun(@(x) D{x}.dog_a, x);
a = a./2; % convert to FW space
% std of DoG
s = arrayfun(@(x) D{x}.dog_std, x);
s = s./2; % convert to FW space

% PLOT
set(gcf, 'Color', [1 1 1]); hold on;

FontSize = 18;
Opts.BarWidth = 0.5;
Opts.Alpha = 0;
Opts.Color = [0.4 0.4 0.4];
do_bar(a, s, x, Opts);

do_fw = 0;
if do_fw 
    load([this.Folder.Stats '/stat_fw_nback.mat'], 'yn', 'yn_std');        
    Opts.BarWidth = 0.08;
    Opts.Color = [1 0.3 0.3];
    do_bar(yn, yn_std, x+0.4, Opts);
    
end

YLim = [-0.04 0.13];

set(gca, ...
    'Box','off', ...
    'YLim', YLim, ...
    'FontSize', FontSize, ...
    'XTick', 0:1:4, ...
    'XTickLabels', {'' '1' '2' '3' ''});

%hy = ylabel('Amplitude of serial dependence (degrees)', 'FontSize', FontSize);
hy = ylabel('Amplitude (radians)', 'FontSize', FontSize);
hx = xlabel('n-th trial back', 'FontSize', FontSize);

% Error bars represent 1 s.d. of the bootstrapped distribution.
% Data in each bar are based on four subjects and 260 data points per subject. (c)

end
