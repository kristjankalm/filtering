function kappa = PlotMNErrorDist(this, e, Opts)

if nargin < 3, Opts = []; end
if ~isfield(Opts, 'Bar'), Opts.Bar = 1; end
if ~isfield(Opts, 'FW'), Opts.FW = 0; end
if ~isfield(Opts, 'Color'), Opts.Color = [0 0 0]; end

%% plot model errors

% load and format errors
if nargin < 2 || isempty(e)
    this.StatsMN;
    load([this.Folder.Stats '/stat_' this.Label '_' this.Data '.mat'], 'E');
    e = E.e;
end

%% plot params
LineWidth = 3;
FontSize = 14;
cols = brewermap(12,'Set1');
set(gcf, 'Color', [1 1 1]);
hold on

x = 0:4;
hy = histc(e, x);

if Opts.Bar
    Opts.Color = [0.2 0.2 0.2];
    Opts.BarWidth = 0.6;
    do_bar(hy, [], x, Opts);
end

set(gca, ...
    'XLim', [-0.5 4.5], ...
    'FontSize', FontSize);

%% fit a Mallows
% vmfun = @(b, x) b(2) .* circ_vmpdf(x, 0, b(1));
% B = nlinfit(x, hy, vmfun, [1 1]);
% kappa_m = round(B(1),1);
% line(x, vmfun(B, x), 'Color', Opts.Color, 'LineWidth', LineWidth);
% 
% %% disp
% if Opts.FW
%     % display std and kappa        
%     std_fw = num2str(round(sqrt(1/kappa_fw),2));
%     std_m  = num2str(round(sqrt(1/kappa_m),2));    
%     text_fw = ['std_E=' std_fw ', \kappa_E=' num2str(kappa_fw) ];
%     text_m =  ['std_E=' std_m  ', \kappa_E=' num2str(kappa_m) ];    
%     text(0.15, 50, text_fw, 'FontSize', 14, 'Color', cols(1,:));
%     text(0.15, 59, text_m,  'FontSize', 14, 'Color', 'k');
% end

%xlabel('Error (radians)');
xlabel('Error (Levenshtein)');
ylabel('Counts');
%title('Histogram of errors');


end
