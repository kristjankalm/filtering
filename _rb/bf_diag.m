clear all, close all

y = -pi : 0.01 : pi;
RQ_ratio = 223; % R from F&W (k=~56 radians) / uniform prior (k = 0.025, std = 2*pi)
RQ_ratio = 5.5795; % R (F&W) / prior that gives ca 8 deg amplitude
%Q = [0 linspace(0.5,1.5,20) RQ_ratio]; % system kappa
Q = 56;
R = 10; % obs/lh kappa

% m = @(x, kappa_1, kappa_2) ...
%     mod(atan2( ...
%     kappa_1 * sin(x), ... 
%     kappa_1 * cos(x) + kappa_2),...
%     2.*pi);

e1 = @(x, kappa_1, kappa_2) ...
    atan(kappa_1 .* sin(x) ./ ... 
    (kappa_1 .* cos(x) + kappa_2));

e = @(x, kappa_1, kappa_2) ...
    atan2(kappa_1 .* sin(x), ... 
    (kappa_1 .* cos(x) + kappa_2));

figure('Position', [100 700 500 500])
hold on;

num_lines = 49;
cols = brewermap(size(Q,2),'RdYlBu');

e = e1(y, R, Q);
plot(y, e, '-'); % error


% text(2,0.5, '\kappa_Q > \kappa_R', 'Fontsize', 20);
% text(2,1.7, '\kappa_R > \kappa_Q', 'Fontsize', 20);
% 
% line([0 pi], [0 pi/2], 'Color', [0 0 0], 'LineWidth', 2)


% set(gca, ...
%     'YLim', [0 pi], ...
%     'XLim', [0 pi]);

xlabel('Distance between prior and likelihood means')
ylabel('Distance between posterior and likelihood means')

% % vm pdf
% vmf = @(kappa, x) 1 ./ (2 .* pi .* besseli(0,kappa)) .* exp(kappa .* cos(x));
% % vm pdf 1st derivative
% vmd = @(kappa, x) -(kappa .* exp(kappa .* cos(x)) .* sin(x))  ./ (2 .* pi .* besseli(0, kappa));
% 
% % gaussian 1st derivative
% nd = @(b, x) x .* b(1) .* b(2) .* (sqrt(2)./exp(-0.5)) .*exp(-(b(2).*x).^2);
% 
% dfun = vmd;
% B = bf.FitErrFunc(y-pi, post_mu, dfun);
% 
% figure(2)
% plot(y-pi, post_mu); hold on
% plot(y-pi, dfun(B, y-pi));
% 
% % set(gca, 'XLim', [0 2*pi])

% multiplication of two posteriors



% C = kappa_1 * cos(x) + kappa_2 * cos(mu_2);
% S = kappa_1 * sin(x) + kappa_2 * sin(mu_2);
% mu_ = mod(atan2(S,C),2*pi);
% kappa_ = sqrt(C^2+S^2);
% vm = VMDistribution(mu_, kappa_);
% 
% m = @(x, kappa_1, mu_2, kappa_2) ...
%     mod(atan2(...
%     kappa_1 * sin(x) + kappa_2 * sin(mu_2), ... 
%     kappa_1 * cos(x) + kappa_2 * cos(mu_2)),...
%     2.*pi);


%%%%%%%%%%%%%%%%

% bf_diff.m

x = linspace(-pi, pi, 100);
k_1 = 1; % Q
k_2 = 1.5;   % R

f = @(x, k_1, k_2) ...
    atan(k_1 .* sin(x) ./ ...
    (k_1 .* cos(x) + k_2));

df = @(x, k_1, k_2) ...
    (( k_1.^2 .* sin(x).^2 ) + ( k_1 .* (k_1 .* cos(x) + k_2) .* cos(x) )) ...
        ./ ...
    ( (k_1.^2 .* sin(x).^2) + (k_1 .* cos(x) + k_2).^2 ) ...
    ;



% 1st deriv = 0
df_0 = pi - acos(k_1/k_2);
alpha = f(df_0, k_1, k_2);


y = f(x, k_1, k_2);
dy = df(x, k_1, k_2);

plot(x, y); hold on
plot(x, dy);

%z = circ_vmpdf(z, 0, 10^-3);

% n = 10^4;
% x = linspace(0, 2*pi, n);
%
% % posterior parameters
% kappa_3 = 1; % kappa for p(z_{t-1})
% %psi = 0.5; % weighting between the nat prior and p(z_{t-1})
%
% % mixture parameters
% %load('mat/girshick.mat', 'b');
%
% b = [0.0215 0.0178 0.8365 0.8728];
% psi = (b(1) + b(2));
% p = [b(1) b(2) psi];
% kappa = [b(3) b(4) kappa_3];
%
% % function handle for a mixture of 3 VM
% cy = @(z, p, kappa) (...
%     ( ...
%         (   p(1) / (2*pi*besseli(0,kappa(1)))     ) ...
%         * exp(kappa(1) * cos(x))...
%     ) + ...
%     ( ...
%         (   p(2) / (2*pi*besseli(0,kappa(2)))     ) ...
%         * exp(kappa(2) * cos(x - pi))...
%     ) + ...
%     (...
%         (   p(3) / (2*pi*besseli(0,kappa(3)))     ) ...
%         * exp(kappa(3) * cos(x - z)) ...
%     ) ...
%     )';
%
% y = cy(z, p, kappa);
%
% z = x(y == max(y));
%
% z = z(1);