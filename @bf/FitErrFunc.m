function [B, p, D] = FitErrFunc(x, y, dfun, p)

% Ordinary Least Squares cost function
ols = @(b) sum((dfun(b,x) - y).^2);
% fitting options
opts = optimset('MaxFunEvals', 10^8, 'MaxIter', 10^6);

% hack to determine the size of parameter in dfun
istwo = sum(strfind(func2str(dfun), '(2)'));
if istwo
    num_params = 2;
else
    num_params = 1;
end

param_start = rand(num_params,1);

% do fit
B = fminsearch(ols, param_start, opts);

% do probability distribution
% shuffle the data labels (relative orientation of the previous trial)
% prob = proportion of simulated values => B
D = []; % null distribution of param values
p = [];
if nargout > 1
    num_shuffle = 10^4;
    num_rows = size(x,1);
    D = nan(num_shuffle,1);
    parfor i = 1:num_shuffle
        xr = x(randperm(num_rows),1);
        olsr = @(b) sum((dfun(b,xr) - y).^2);
        d = fminsearch(olsr, param_start, opts);
        D(i,1) = d(end);
    end
    p = sum(abs(D)>=B) ./ num_shuffle;
end


end

